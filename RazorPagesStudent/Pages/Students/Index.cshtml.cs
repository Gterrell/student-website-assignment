using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using RazorPagesStudent.Models;

namespace RazorPagesStudent.Pages.Students
{
    public class IndexModel : PageModel
    {
        private readonly RazorPagesStudent.Models.StudentContext _context;

        public IndexModel(RazorPagesStudent.Models.StudentContext context)
        {
            _context = context;
        }

        public IList<Student> Student { get;set; }

        public async Task OnGetAsync()
        {
            Student = await _context.Student.ToListAsync();
        }
    }
}
