﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RazorPagesStudent.Models
{
    public class Student
    {

        public int ID { get; set; }
        public string Name { get; set; }
        public string YearOfStudy { get; set; }
        public decimal Grade { get; set; }
        public DateTime GraduationDate { get; set; }


    }
}
